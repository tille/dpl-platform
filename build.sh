#!/bin/sh
#
# Dependencies: html2wml, html2text
#
# By Andreas Tille <tille@debian.org>
#  (originally found at https://salsa.debian.org/jcc/dpl-platform
#   by Jonathan Carter <jcc@debian.org>)
#
# I consider this script trivial and assert no copyright,

set -x
VERSION=$(git describe --tags --abbrev=0)
DATE=$(date +"%Y-%m-%d")

# Plain version for debian website:
cat wmlheader.html  > platform.wml
sed -e "s/VERSION/${VERSION}/g" -e "s/DATE/${DATE}/" content.html >> platform.wml

# More readable version for mine:
cat style.css > platform.html
sed -e "s/:##}//g" -e "s/VERSION/${VERSION}/g" -e "s/DATE/${DATE}/" content.html >> platform.html

# Plaintext version:
# Plaintext version:
echo "This plaintext version is generated from platform.html" > README.md
echo "using html2text, git tag $VERSION." >> README.md
html2text -style pretty -width 78 platform.html | tail -n +3 >> README.md
#pandoc -f html -t plain -o README.txt platform.html
